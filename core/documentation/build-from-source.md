# Build from Source

This document describes how to build the JobAid Core Service from the command line and how to import the JobAid Core Service projects into an IDE.

The JobAid Core Service uses a Gradle build. The instructions below use the Gradle Wrapper from the root of the source tree. The wrapper script serves as a cross-platform, self-contained bootstrap mechanism for the build system.

## Before You Start

To build you will need Git and [JDK 11](https://adoptium.net/). Be sure that your JAVA_HOME environment variable points to the jdk11 folder extracted from the JDK download.

## Get the Source Code

```bash
git clone git@gitlab.com:jobaid/base.git
cd core
```

## Build from the Command Line

To compile, test and build all jars, distribution zips and docs use:

```bash
./gradlew build
```

The first time you run the build it may take a while to download Gradle and all build dependencies, as well as to run all tests. Once you've bootstrapped a Gradle distribution and downloaded dependencies, those are cached in your $HOME/.gradle directory.

## Import into your IDE

Ensure JDK 11 is configured properly in the IDE. Follow instructions for [IntelliJ IDEA](import-into-idea.md).
