package com.jobaidukraine.core.adapter.out.db.implementation.match;

import com.google.common.collect.ImmutableList;
import com.jobaidukraine.core.adapter.in.rest.mapper.MatchMapper;
import com.jobaidukraine.core.adapter.out.db.entities.AddressEntity;
import com.jobaidukraine.core.adapter.out.db.entities.ClassificationEntity;
import com.jobaidukraine.core.adapter.out.db.entities.ExperienceEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.LanguageLevelEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.OutDbMatchMapperImpl;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.Address;
import com.jobaidukraine.core.domain.Experience;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.JobType;
import com.jobaidukraine.core.domain.LanguageLevel;
import com.jobaidukraine.core.domain.Match;
import com.jobaidukraine.core.domain.Role;
import com.jobaidukraine.core.domain.User;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MatchAdapterTest {

    @Mock
    private JobRepository jobRepositoryMock;

    private MatchAdapter testSubject;

    @BeforeEach
    void setUp() {
        testSubject = new MatchAdapter(jobRepositoryMock, new OutDbMatchMapperImpl());
    }

    @Test
    void shouldReturnNoMatchesWhenNoJobsExists() {
        when(jobRepositoryMock.findAll()).thenReturn(ImmutableList.of());
        final Pageable unpaged = Pageable.unpaged();
        final User user = createDefaultUser();

        final Page<Match> result = testSubject.match(unpaged, user);

        assertThat(result).isEmpty();
    }

    @Test
    void shouldReturnNoMatchesWhenNoCriteriaMatch() {
        when(jobRepositoryMock.findAll()).thenReturn(ImmutableList.of(createDefaultJob()));
        final Pageable unpaged = Pageable.unpaged();
        final User user = createDefaultUser();

        final Page<Match> result = testSubject.match(unpaged, user);

        assertThat(result).isEmpty();
    }

    JobEntity createDefaultJob() {
        final Long id = 1L;
        final LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
        final LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
        final Integer version = 1;
        final Boolean deleted = false;
        final String title = "test title";
        final String description = "test description";
        final String jobType = JobType.FREELANCE.name();
        final String applicationLink = "test-applicationlink.test";
        final String applicationMail = "test@test.test";
        final String videoUrl = "test-videourl.test";
        final AddressEntity address = new AddressEntity();
        final Set<ExperienceEntity> qualifications = new HashSet<>(Collections.emptySet());
        final ClassificationEntity classification = null;
        final Set<LanguageLevelEntity> languageLevels = new HashSet<>(Collections.emptySet());
        return new JobEntity(
                id,
                createdAt,
                updatedAt,
                version,
                deleted,
                title,
                description,
                jobType,
                applicationLink,
                applicationMail,
                videoUrl,
                address,
                qualifications,
                classification,
                languageLevels);
    }

    private User createDefaultUser() {
        final Long id = 1L;
        final LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
        final LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
        final Integer version = 1;
        final Boolean deleted = false;
        final String email = "test@test.test";
        final String firstname = "test firstname";
        final String lastname = "test lastname";
        final String position = "test position";
        final String country = "test country";
        final Role role = Role.USER;
        final Experience experience = Experience.NEW_GRADUATE;
        final Set<LanguageLevel> languageLevels = new HashSet<>();
        return new User(
                id,
                createdAt,
                updatedAt,
                version,
                deleted,
                email,
                firstname,
                lastname,
                position,
                country,
                role,
                experience,
                languageLevels);
    }
}
