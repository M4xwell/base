package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.MatchResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.MatchDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.MatchMapper;
import com.jobaidukraine.core.adapter.out.db.entities.ExperienceEntity;
import com.jobaidukraine.core.domain.Experience;
import com.jobaidukraine.core.domain.LanguageLevel;
import com.jobaidukraine.core.domain.Role;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.in.queries.UserQuery;
import com.jobaidukraine.core.services.ports.in.usecases.MatchUseCase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureMockMvc
@WebMvcTest(MatchController.class)
@WithMockUser(
        username = "Picard",
        roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class MatchControllerTest {

    @Autowired
    MockMvc mock;

    @MockBean
    UserQuery userQuery;

    @MockBean
    MatchResourceAssembler matchResourceAssembler;

    @MockBean
    MatchUseCase matchUseCase;

    @MockBean
    PagedResourcesAssembler<MatchDto> pagedResourcesAssembler;

    @MockBean
    MatchMapper matchMapper;

    static final String MATCH_BASE_URL = "/v1.0/matches";

    @Test
    void checkFieldsAllCriteriaNotFound() throws Exception {
        User user = getMockUser("", null);

        when(userQuery.findById(1L)).thenReturn(user);
        when(matchUseCase.checkFields(user)).thenReturn(Arrays.asList("country", "experience", "language"));

        mock.perform(get(MATCH_BASE_URL + "/{id}", 1L).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0]").value("country"))
                .andExpect(jsonPath("$[1]").value("experience"))
                .andExpect(jsonPath("$[2]").value("language"))
                .andExpect(jsonPath("$").isArray());
    }

    @Test
    void checkFieldsAllCriteriaFound() throws Exception {
        User user = getMockUser("germany", Experience.STUDENT);

        when(userQuery.findById(1L)).thenReturn(user);
        when(matchUseCase.checkFields(user)).thenReturn(new ArrayList<>());

        mock.perform(get(MATCH_BASE_URL + "/{id}", 1L).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$").isEmpty());
    }

    User getMockUser(final String c, final Experience e) {
        Long id = 1L;
        LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
        LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
        Integer version = 1;
        Boolean deleted = false;
        String email = "test@test.test";
        String firstname = "test firstname";
        String lastname = "test lastname";
        String position = "test position";
        String country = c;
        Role role = Role.USER;
        Experience experience = e;
        Set<LanguageLevel> languageLevels = new HashSet<>();
        return new User(
                id,
                createdAt,
                updatedAt,
                version,
                deleted,
                email,
                firstname,
                lastname,
                position,
                country,
                role,
                experience,
                languageLevels);
    }

}
