package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Experience;
import com.jobaidukraine.core.domain.LanguageLevel;
import com.jobaidukraine.core.domain.Role;
import com.jobaidukraine.core.domain.User;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class MatchServiceTest {

    @InjectMocks
    private MatchService matchService;

    @Test
    void checkFields() {
        User user = getMockUser();

        List<String> criteria = matchService.checkFields(user);

        assertEquals(criteria.size(), 3);
        assertEquals(criteria.get(0), "country");
        assertEquals(criteria.get(1), "experience");
        assertEquals(criteria.get(2), "language");
    }

    User getMockUser() {
        Long id = 1L;
        LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
        LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
        Integer version = 1;
        Boolean deleted = false;
        String email = "test@test.test";
        String firstname = "test firstname";
        String lastname = "test lastname";
        String position = "test position";
        String country = "";
        Role role = Role.USER;
        Experience experience = null;
        Set<LanguageLevel> languageLevels = new HashSet<>();
        return new User(
                id,
                createdAt,
                updatedAt,
                version,
                deleted,
                email,
                firstname,
                lastname,
                position,
                country,
                role,
                experience,
                languageLevels);
    }
}
