package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Role;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.in.queries.UserQuery;
import com.jobaidukraine.core.services.ports.in.usecases.UserUseCase;
import com.jobaidukraine.core.services.ports.out.UserPort;
import java.util.Optional;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService implements UserQuery, UserUseCase {

  private final UserPort userPort;

  @Override
  public User save(User user) {
    user.setRole(Role.USER);
    return this.userPort.save(user);
  }

  @Override
  public Page<User> findAllByPageable(Pageable pageable) {
    return this.userPort.findAllByPageable(pageable);
  }

  @Override
  public User findById(long id) {
    return this.userPort.findById(id).orElseThrow();
  }

  @Override
  public User update(User user) {
    return userPort.update(user);
  }

  @Override
  public void delete(long id) {
    this.userPort.delete(id);
  }

  @Override
  public Optional<User> findByEmail(String email) {
    return this.userPort.findByEmail(email);
  }
}
