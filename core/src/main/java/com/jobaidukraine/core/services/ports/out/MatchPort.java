package com.jobaidukraine.core.services.ports.out;

import com.jobaidukraine.core.domain.Match;
import com.jobaidukraine.core.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface MatchPort {
    Page<Match> match(Pageable pageable, User user);
}
