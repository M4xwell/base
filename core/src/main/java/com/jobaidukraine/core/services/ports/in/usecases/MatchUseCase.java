package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.Match;
import com.jobaidukraine.core.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface MatchUseCase {

    List<String> checkFields(User user);

    Page<Match> match(Pageable pageable, User user);
}
