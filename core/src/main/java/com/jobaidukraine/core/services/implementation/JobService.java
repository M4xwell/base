package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.services.ports.in.queries.JobQuery;
import com.jobaidukraine.core.services.ports.in.usecases.JobUseCase;
import com.jobaidukraine.core.services.ports.out.CompanyPort;
import com.jobaidukraine.core.services.ports.out.JobPort;
import java.util.Comparator;
import java.util.stream.Collectors;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class JobService implements JobQuery, JobUseCase {

  private final JobPort jobPort;
  private final CompanyPort companyPort;

  @Override
  public Job save(Job job, Long companyId) {
    Company company = companyPort.findById(companyId).orElseThrow();
    company.getJobs().add(job);
    company = companyPort.save(company);
    return company.getJobs().stream()
        .sorted(Comparator.comparing(Job::getCreatedAt).reversed())
        .collect(Collectors.toList())
        .get(0);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable) {
    return this.jobPort.findAllByPageable(pageable);
  }

  @Override
  public Page<Job> findAllByPageable(Pageable pageable, String title) {
    return this.jobPort.findAllByPageable(pageable, title);
  }

  @Override
  public Job findById(long id) {
    return this.jobPort.findById(id).orElseThrow();
  }

  @Override
  public Job update(Job job) {
    return this.jobPort.update(job);
  }

  @Override
  public void delete(long id) {
    jobPort.delete(id);
  }
}
