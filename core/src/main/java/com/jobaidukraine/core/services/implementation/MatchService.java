package com.jobaidukraine.core.services.implementation;

import com.jobaidukraine.core.domain.Match;
import com.jobaidukraine.core.domain.MatchCriteria;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.in.usecases.MatchUseCase;
import com.jobaidukraine.core.services.ports.out.MatchPort;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class MatchService implements MatchUseCase {

    private final MatchPort matchPort;

    @Override
    public List<String> checkFields(User user) {
        List<String> missingFields = new ArrayList();

        if (user.getCountry().isEmpty()) {
            missingFields.add(MatchCriteria.COUNTRY.toString().toLowerCase());
        }

        if (user.getExperience() == null) {
            missingFields.add(MatchCriteria.EXPERIENCE.toString().toLowerCase());
        }

        if (user.getLanguageLevels().isEmpty()) {
            missingFields.add(MatchCriteria.LANGUAGE.toString().toLowerCase());
        }

        return missingFields;
    }

    @Override
    public Page<Match> match(final Pageable pageable, final User user) {
        return matchPort.match(pageable, user);
    }
}
