package com.jobaidukraine.core.domain;

public enum MatchCriteria {
    COUNTRY,
    EXPERIENCE,
    LANGUAGE
}
