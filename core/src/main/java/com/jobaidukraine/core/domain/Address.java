package com.jobaidukraine.core.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Address {
  private Float latitude;
  private Float longitude;
  private String street;
  private String houseNumber;
  private String zip;
  private String city;
  private String country;
}
