package com.jobaidukraine.core.domain;

public enum JobType {
  FREELANCE("FREELANCE"),
  PERMANENT("PERMANENT"),
  PART_TIME("PART_TIME"),
  TEMPORARY("TEMPORARY");

  final String type;

  JobType(String type) {
    this.type = type;
  }

  public String getType() {
    return type;
  }
}
