package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Job {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;

  private String title;
  private String description;
  private JobType jobType;
  private String applicationLink;
  private String applicationMail;
  private String videoUrl;

  private Address address;
  private Set<Experience> qualifications;
  private Classification classification;
  private Set<LanguageLevel> languageLevels;
}
