package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.Match;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
        componentModel = "spring",
        implementationName = "OutDbMatchMapperImpl",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        uses = {AddressMapper.class, LanguageMapper.class})
public interface MatchMapper {

    Match toDomain(JobEntity jobEntity);

    JobEntity toEntity(Match job);
}
