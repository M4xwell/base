package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.MatchResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.MatchDto;
import com.jobaidukraine.core.adapter.in.rest.dto.UserDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.MatchMapper;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.in.queries.UserQuery;
import com.jobaidukraine.core.services.ports.in.usecases.MatchUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/v1.0/matches")
@RequiredArgsConstructor
@OpenAPIDefinition(
        info =
        @Info(
                title = "JobAidUkraine",
                version = "1.0",
                description = "JobAidUkraine REST API",
                license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
        tags = {@Tag(name = "Match")})
public class MatchController {

    private final UserQuery userQuery;
    private final MatchResourceAssembler matchResourceAssembler;
    private final MatchUseCase matchUseCase;
    private final PagedResourcesAssembler<MatchDto> pagedResourcesAssembler;
    private final MatchMapper matchMapper;

    @Operation(
            summary = "Show missing match fields for a user by id",
            tags = {"Match"})
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Match fields retrieval was successful",
                            content =
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = UserDto.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
                    @ApiResponse(responseCode = "401", description = "Unauthorized"),
                    @ApiResponse(responseCode = "404", description = "User not found"),
            })
    @GetMapping(value = "/{id}")
    @PreAuthorize(
            "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
    public List<String> checkFields(@PathVariable long id) {
        User user = this.userQuery.findById(id);
        return matchUseCase.checkFields(user);
    }

    @Operation(
            summary = "Get matches",
            tags = {"Match"})
    @ApiResponses(
            value = {
                    @ApiResponse(
                            responseCode = "200",
                            description = "Match retrieval was successful",
                            content =
                            @Content(
                                    mediaType = "application/json",
                                    schema = @Schema(implementation = MatchDto.class))),
                    @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
                    @ApiResponse(responseCode = "404", description = "Job not found")
            })
    @GetMapping(value = "/match")
    public PagedModel<EntityModel<MatchDto>> search(final Pageable pageable) {
        PagedModel<EntityModel<MatchDto>> results = PagedModel.empty();

        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();

        if (principal instanceof User) {
            Page<MatchDto> matches = this.matchUseCase.match(pageable, (User)principal).map(matchMapper::domainToDto);

            results = pagedResourcesAssembler.toModel(matches, matchResourceAssembler);
        }

        return results;
    }
}