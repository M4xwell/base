package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.MatchDto;
import com.jobaidukraine.core.domain.Match;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
        componentModel = "spring",
        implementationName = "InRestMatchMapperImpl",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR,
        uses = {AddressMapper.class, LanguageMapper.class})
public interface MatchMapper {

    Match dtoToDomain(MatchDto MatchDto);

    MatchDto domainToDto(Match match);
}