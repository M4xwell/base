package com.jobaidukraine.core.adapter.out.db.implementation.match;

import com.jobaidukraine.core.adapter.out.db.entities.ExperienceEntity;
import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.adapter.out.db.entities.LanguageLevelEntity;
import com.jobaidukraine.core.adapter.out.db.mapper.MatchMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.JobRepository;
import com.jobaidukraine.core.domain.Experience;
import com.jobaidukraine.core.domain.LanguageLevel;
import com.jobaidukraine.core.domain.Match;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.out.MatchPort;
import lombok.RequiredArgsConstructor;
import org.hibernate.internal.util.StringHelper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hibernate.internal.util.StringHelper.isEmpty;

@Service
@RequiredArgsConstructor
public class MatchAdapter implements MatchPort {

    private final JobRepository jobRepository;
    private final MatchMapper matchMapper;

    @Override
    public Page<Match> match(final Pageable pageable, final User user) {
        final List<JobEntity> all = jobRepository.findAll();

        final List<Match> matches = all.stream()
                .filter(job -> job.getAddress() != null)
                .filter(job -> matchCountry(job.getAddress().getCountry(), user.getCountry()))
                .filter(job -> matchExperience(job.getQualifications(), user.getExperience()))
                .filter(job -> matchLanguageLevels(job.getLanguageLevels(), user.getLanguageLevels()))
                .map(matchMapper::toDomain)
                .collect(Collectors.toList());

        // TODO: Total via property
        return new PageImpl<>(matches, pageable, 100);
    }

    private boolean matchCountry(final String jobCountry, final String userCountry) {
        if (isEmpty(jobCountry) || isEmpty(userCountry)) {
            return false;
        }

        return jobCountry.equals(userCountry);
    }

    private boolean matchExperience(final Set<ExperienceEntity> jobExperiences, final Experience userExperience) {
        return jobExperiences.stream().anyMatch(jobExperience -> {
            if (jobExperience == null || userExperience == null) {
                return false;
            }

            return jobExperience.name().equals(userExperience.name());
        });
    }

    private boolean matchLanguageLevels(final Set<LanguageLevelEntity> jobLanguageLevels, final Set<LanguageLevel> userLanguageLevels) {
        return jobLanguageLevels.stream()
                .anyMatch(jobLanguageLevel -> userLanguageLevels.stream()
                        .anyMatch(userLanguageLevel ->
                                jobLanguageLevel.getLanguage().equals(userLanguageLevel.getLanguage())
                                        && jobLanguageLevel.getLanguageSkill().name().equals(userLanguageLevel.getLanguageSkill().name())));
    }
}
