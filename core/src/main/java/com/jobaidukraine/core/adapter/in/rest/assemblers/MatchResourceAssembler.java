package com.jobaidukraine.core.adapter.in.rest.assemblers;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import com.jobaidukraine.core.adapter.in.rest.controller.JobController;
import com.jobaidukraine.core.adapter.in.rest.controller.MatchController;
import com.jobaidukraine.core.adapter.in.rest.dto.MatchDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MatchResourceAssembler
        implements RepresentationModelAssembler<MatchDto, EntityModel<MatchDto>> {

    @Override
    public EntityModel<MatchDto> toModel(MatchDto match) {

        try {
            return EntityModel.of(
                    match,
                    linkTo(methodOn(JobController.class).show(match.getId())).withSelfRel());

        } catch (Exception e) {
            log.error(e.getMessage());
        }

        return null;
    }
}
