package com.jobaidukraine.core.adapter.out.db.entities;

public enum JobTypeEntity {
  FREELANCE,
  PERMANENT,
  PART_TIME,
  TEMPORARY
}
