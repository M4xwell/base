plugins {
	id 'org.springframework.boot' version '2.6.6'
	id 'io.spring.dependency-management' version '1.0.11.RELEASE'
	id 'java'
 	id 'com.diffplug.spotless' version '6.4.2'
	id 'org.sonarqube' version '3.3'
	id 'jacoco'
}

group = 'com.jobaidukraine'
version = '0.0.1-SNAPSHOT'
sourceCompatibility = '11'

jacocoTestReport {
	afterEvaluate {
		classDirectories.setFrom(files(classDirectories.files.collect {
			fileTree(dir: it, exclude: [
					"**/*Dto.*",
					"**/*Entity.*",
					"**/configuration/**/*.*",
					"**/domain/*.*",
			])
		}))
	}

	reports {
		xml.enabled true
	}
}

sonarqube {
	properties {
		property 'sonar.jacoco.reportPath', 'core/build/reports/jacoco/test/jacocoTestReport.xml'
		property 'sonar.coverage.exclusions', '**/*Dto.*, **/*Entity.*, **/domain/*.*, **/configuration/**/*.*'
		property 'sonar.cpd.exclusions', '**/*Dto.*'
		property 'sonar.projectKey', 'jobaid_base'
		property 'sonar.organization', 'jobaid'
		property 'sonar.host.url', 'https://sonarcloud.io'
		property 'sonar.login', System.getenv('SONAR_TOKEN')
	}
}

spotless {
	java {
		importOrder()
		removeUnusedImports()
		googleJavaFormat()
		trimTrailingWhitespace()
	}
}

repositories {
	maven { url 'https://repo.spring.io/release' }
	mavenCentral()
}

dependencies {
	//Security
	implementation 'org.springframework.boot:spring-boot-starter-security'
	implementation 'org.springframework.security:spring-security-oauth2-resource-server'
	implementation group: 'com.nimbusds', name: 'nimbus-jose-jwt', version: '9.9'
	implementation 'org.springframework.security:spring-security-oauth2-jose:5.6.3'
	implementation 'org.springframework.cloud:spring-cloud-gcp-starter-security-iap:1.2.8.RELEASE'

	//SQL
	implementation 'org.springframework.boot:spring-boot-starter-data-jdbc'
	implementation 'org.springframework.boot:spring-boot-starter-data-jpa'
	runtimeOnly 'org.mariadb.jdbc:mariadb-java-client'

	//Web
	implementation 'org.springframework.boot:spring-boot-starter-web'
	implementation 'org.springframework.boot:spring-boot-starter-hateoas'
	implementation 'org.springframework.boot:spring-boot-starter-webflux'

	//Testing
	testImplementation 'org.springframework.boot:spring-boot-starter-test'
	testImplementation 'org.springframework.security:spring-security-test'
	testImplementation 'org.junit.jupiter:junit-jupiter-api:5.8.2'
	testImplementation 'org.junit.jupiter:junit-jupiter-engine:5.8.2'
	testImplementation 'org.junit.platform:junit-platform-runner:1.8.2'
	testImplementation 'org.junit.platform:junit-platform-launcher:1.8.2'
	testImplementation 'com.tngtech.archunit:archunit-junit5-engine:0.23.1'
	testImplementation 'org.mockito:mockito-core:4.4.0'
	implementation 'org.springdoc:springdoc-openapi-data-rest:1.6.7'
	implementation 'org.springdoc:springdoc-openapi-hateoas:1.6.7'
	implementation 'org.springdoc:springdoc-openapi-native:1.6.7'
	implementation 'org.springdoc:springdoc-openapi-security:1.6.7'
	implementation 'org.springdoc:springdoc-openapi-ui:1.6.7'
	implementation 'com.fasterxml.jackson.core:jackson-databind'

	//Developer Tools
	compileOnly 'org.projectlombok:lombok'
	annotationProcessor 'org.projectlombok:lombok'
	implementation 'org.mapstruct:mapstruct:1.4.2.Final'
	annotationProcessor 'org.mapstruct:mapstruct-processor:1.4.2.Final'
	developmentOnly 'org.springframework.boot:spring-boot-devtools'

	//Ops
	implementation 'org.springframework.boot:spring-boot-starter-actuator'
}

tasks.named('test') {
	useJUnitPlatform()
	finalizedBy("jacocoTestReport")
}

tasks.named('bootBuildImage') {
	builder = 'paketobuildpacks/builder:tiny'
	environment = ['BP_NATIVE_IMAGE': 'true']
}
