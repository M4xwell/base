import fs from 'fs';
import express from 'express';
import cors from 'cors';

const file = JSON.parse(fs.readFileSync('plz-5stellig-centroid.geojson', { encoding: 'utf-8' }));
const app = express()
const port = 3001

const corsAllowed = ['http://localhost:3000', 'https://beta.jobaidukraine.com']

app.use(cors());

const parseResults = (result) => {
    return result.map(element => {
        return {
            lat: element.geometry.coordinates[0],
            lng: element.geometry.coordinates[1],
            town: element.properties.note.slice(6)
        }
    })
}

const findTown = (town) => {
    file.features.filter(entry => {
        if (entry.properties.note.toLowerCase().indexOf(town) > -1) {
            return parseResults(entry);
        }
    });
}

/* Radius of earth in kilometers */
const EARTH_RADIUS = 6371;

const distance = (from, to) => {
    /* Convert to radians */
    let lat1 = from.lat * Math.PI /180;
    let lng1 = from.lng * Math.PI / 180;

    let lat2 = to.lat * Math.PI / 180;
    let lng2 = to.lng * Math.PI / 180;

    /* Haversine calculation (https://en.wikipedia.org/wiki/Haversine_formula) */
    let delta_lat = Math.abs(lat2 - lat1);
    let delta_long = Math.abs(lng2 - lng1);

    let a = Math.pow(Math.sin(delta_lat / 2), 2) +
        Math.cos(lat1) * Math.cos(lat2) *
        Math.pow(Math.sin(delta_long / 2), 2);

    let c = 2 * Math.asin(Math.sqrt(a));

    return c * EARTH_RADIUS;
}

app.get('/zip/:zip', (req, res) => {

    if(req.params.zip.length <3) {
        res.json({'error':'Minimum 3 characters are required for parameter.'});
        return;
    }
    let result = file.features.filter(entry => {
        if (entry.properties.plz.indexOf(req.params.zip) > -1) {
            return entry;
        }
    });
    res.json(parseResults(result));
});

app.get('/town/:town', (req, res) => {

    if(req.params.town.length <3) {
        res.json({'error':'Minimum 3 characters are required for parameter.'});
        return;
    }

    const result = findTown(req.params.town);

    res.json(parseResults(result));
});

app.get('/distance/:town_from/:town_to'), (req, res) => {
    if(req.params.town_from.length <3 || req.params.town_to.length <3) {
        res.json({'error':'Minimum 3 characters are required for a town as parameter.'});
        return;
    }

    const result_from = findTown(req.params.town_from);
    const result_to = findTown(req.params.town_to);
    
    return { 
        from: result_from,
        to: result_to,
        distance: distance(result_from, result_to)
    };
}

app.listen(port, () => {
    console.log(`Geo Service listening on port ${port}`)
})



